package tk.aalkhodiry.gradle.apt

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.ProjectConfigurationException
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.tasks.bundling.AbstractArchiveTask

class AptPlugin implements Plugin<Project> {
    void apply(Project project) {
        if (! project.plugins.findPlugin("java")) {
            throw new ProjectConfigurationException("The java plugin must be applied to the project", null)
        }

        def aptConfiguration = project.configurations.create('apt').extendsFrom(project.configurations.compile)

        project.extensions.create("apt", AptExtension)

        project.afterEvaluate {
            if (project.apt.disableDiscovery() && !project.apt.processors()) {
                throw new ProjectConfigurationException('android-apt configuration error: disableDiscovery may only be enabled in the apt configuration when there\'s at least one processor configured', null);
            }
            configureVariant(project, aptConfiguration, project.apt)
        }
    }

    static void configureVariant(def project, def aptConfiguration, def aptExtension) {
        if (aptConfiguration.empty) {
            project.logger.info("No apt dependencies for configuration ${aptConfiguration.name}");
            return;
        }

        def javaCompile = project.compileJava;
        def aptOutputDir = project.file(new File(project.buildDir, "generated/source/apt"))
        def aptOutput = new File(aptOutputDir, "release")
        def processorPath = aptConfiguration.getAsPath();

        def processors = aptExtension.processors()

        javaCompile.options.compilerArgs += [
                '-s', aptOutputDir
        ]

        if (processors) {
            javaCompile.options.compilerArgs += [
                    '-processor', processors
            ]
        }

        if (!(processors && aptExtension.disableDiscovery())) {
            javaCompile.options.compilerArgs += [
                    '-processorpath', processorPath
            ]
        }

        aptExtension.aptArguments.project = project

        def projectDependencies = aptConfiguration.allDependencies.withType(ProjectDependency.class)

        // There must be a better way, but for now grab the tasks that produce some kind of archive and make sure those
        // run before this javaCompile. Packaging makes sure that processor meta data is on the classpath
        projectDependencies.each { p ->
            def archiveTasks = p.dependencyProject.tasks.withType(AbstractArchiveTask.class)
            archiveTasks.each { t -> javaCompile.dependsOn t.path }
        }

        javaCompile.options.compilerArgs += aptExtension.arguments()

        javaCompile.doFirst {
            aptOutputDir.mkdirs()
        }
    }
}
